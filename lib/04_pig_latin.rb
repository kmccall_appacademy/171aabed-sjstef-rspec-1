def translate(string)
  words = string.split(" ")
  words.map!{|word| latinize(word)}
  words.join(" ")
end

def latinize(word)
  vowels = "aeiou"
  word_let = word.chars
  vow_idx = []
  word_let.each_with_index do |letter, idx|
      next if letter == "u" && word_let[idx-1] == "q"
      vow_idx << idx if vowels.include?(letter)
  end
  
  first_vow = vow_idx.compact.min
  word = word[first_vow..-1]+word[0...first_vow]+"ay"
  word
end 
