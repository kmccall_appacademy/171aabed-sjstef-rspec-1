def echo(word)
    "#{word}"
end

def shout(word)
    "#{word.upcase}"
end

def repeat(word, num=2)
    words = []
    1.upto(num) do
        words << word
    end
    words.join(" ")
end

def start_of_word(word, num)
    letters = []
    word.chars[0..num-1].each do |letter|
        letters << letter
    end
    letters.join
end

def first_word(word)
   word.split(' ')[0] 
end

def titleize(title)
    words = title.split
    littles = ["for", "and", "the", "over"]
    
    words.each_with_index do |word, idx|
        if idx > 0 && littles.include?(word) == true
            word
        else
            words[idx] = word.capitalize
        end
    end
    
    words.join(" ")
end
