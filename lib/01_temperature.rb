def ftoc(farenheit)
	if farenheit == farenheit.to_f
		(farenheit - 32.0) * (5.0/9.0)
	else
		((farenheit - 32) * 5) / 9
	end
end

def ctof(celcius)
	if celcius == celcius.to_f
		((celcius * 9.0) / 5.0) + 32.0
	else
		((celcius * 9) / 5) + 32
	end
end