def add(num1, num2=0)
	num1.to_i + num2.to_i
end

def subtract(num1, num2=0)
	num1.to_i - num2.to_i
end

def sum(array)
	return 0 if array.empty? == true
	array.reduce(:+)
end

def multiply(array)
    array.reduce(:*)
end

def power(num, exponent)
    num ** exponent
end

def factorial(num)
   return 1 if num == 0
   product = 1
   
   1.upto(num) do |n|
       product = product * n
   end
   
   product
end
